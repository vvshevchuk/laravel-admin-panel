@extends('layouts.main_layout')

@section('main_content')
    <article>
        <section id="se1">
            <div class="se1-container">
                <img id="se1-logo" src="css/images/se1-logo.jpg" alt="Logo">
            </div>
            <div id="se1-2-container" class="se1-container">
                <div id="se1-right-text">
                    <p id="se1-right-p">
                        Якщо пригледітись, то неозброєнним оком видно, що математика навколо нас.
                        З давніх давен вона служить людям надійною підмогою в розрахунках,
                        допомагає навігаторам визначати положення судна в морі,
                        землемірам - вимірювати земельні ділянки, астрономам - складати календарі.
                        З математикою ми зустрічаємося всюди, на кожному кроці, з ранку і до вечора.
                        Прокидаючись, ми дивимося на годинник; в трамваї чи тролейбусі потрібно розрахуватися за проїзд;
                        щоб зробити покупку в магазині, потрібно знову виконати грошові розрахунки, і багато іншого.
                        Без математики не можна було б вивчити ні фізику, ні географію, ні креслення.
                    </p>
                    <a href="{{ route('intro') }}" class="se-button-detail"><span>Детальніше</span></a>
                </div>
            </div>
        </section>
        <section id="se2">

            <div id="se2-80">
                <div id="se2-left-text">
                    <h2 id="se2-h2">ІСТОРІЯ МАТЕМАТИКИ</h2>
                    <br>
                    <p class="se-main-text-p">
                        Математика виникла з давніх-давен з практичних потреб людини, її зміст і характер з часом
                        змінювались.
                        Спочатку були предметні уявлення про ціле додатнє число, про відрізок прямої, як найкоротшу
                        відстань між двома точками.
                        Математика пройшла довгий шлях розвитку, перш ніж стала абстрактною наукою з точно сформованими
                        поняттями і методами.
                        Нові вимоги сучасності, розширюють її обсяг, наповнюючи новим змістом старі поняття.
                    </p>
                    <br>
                    <hr class="se-hr">
                    <br>
                    <p class="se-quote-p">
                        Рано чи пізно будь-яка правильна математична ідея знаходить застосування в тій чи іншій справі.
                        <br>
                        <span class="se-quote-author">О.М. Крилов</span>
                    </p>
                    <br>
                    <a href="{{ route('history') }}" class="se-button-detail"><span>Детальніше</span></a>
                </div>
                <div id="se2-img-container">
                    <div id="se2-img-container-1">
                        <img src="css/images/se2-rock-wall.jpg">
                        <img src="css/images/se2-pifagor.jpg">
                    </div>
                    <div id="se2-img-container-2">
                        <img src="css/images/se2-techno.png">
                    </div>
                    <div class="div-clear">
                    </div>
                </div>
                <div class="div-clear">
                </div>
            </div>
        </section>
        <section id="se3-main">
            <div class="se3">
                <div class="se3-block">
                    <h5 class="se3-block-h5">Числа</h5>
                    <img class="se3-block-img" src="css/images/numbers.jpg">
                    <p class="se3-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se3-block-button">Перейти</button>
                </div>
                <div class="se3-block">
                    <h5 class="se3-block-h5">Рівняння</h5>
                    <img class="se3-block-img" src="css/images/trojan-horse.jpg">
                    <p class="se3-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se3-block-button">Перейти</button>
                </div>
                <div class="se3-block">
                    <h5 class="se3-block-h5">Системи рівнянь</h5>
                    <img class="se3-block-img" src="css/images/school-packbag.jpg">
                    <p class="se3-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se3-block-button">Перейти</button>
                </div>
            </div>
            <div class="se3">
                <div id="se3-block-button-container" class="se3-block">
                    <button id="se3-block-button-detail" class="se3-block-button">Читати більше алгебри</button>
                </div>
            </div>
        </section>
        <section id="se4-main">
            <div class="se4">
                <div class="se4-block">
                    <img class="se4-block-img" src="css/images/geometry-icon-design-tool.png">
                    <h5 class="se4-block-h5">Основні геометичні фігури на площині</h5>
                    <p class="se4-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se4-block-button">Перейти</button>
                </div>
                <div class="se4-block">
                    <img class="se4-block-img" src="css/images/geometry-icon-vector.png">
                    <h5 class="se4-block-h5">Вектори</h5><br/>
                    <p class="se4-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se4-block-button">Перейти</button>
                </div>
            </div>
            <div class="se4">
                <div class="se4-block">
                    <img class="se4-block-img" src="css/images/geometry-icon-ruler.png">
                    <h5 class="se4-block-h5">Пряма та площина</h5><br>
                    <p class="se4-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se4-block-button">Перейти</button>
                </div>
                <div class="se4-block">
                    <img class="se4-block-img" src="css/images/geometry-icon-prism.png">
                    <h5 class="se4-block-h5">Призма</h5><br>
                    <p class="se4-block-p">
                        Число є одним з найголовніших об'єктів математики,
                        який використовується для підрахунку, вимірювання та маркування.
                        Символи, які використовуються для позначення чисел називаються цифрами.
                    </p>
                    <button class="se4-block-button">Перейти</button>
                </div>
            </div>
            <div class="se3">
                <div id="se3-block-button-container" class="se3-block">
                    <button id="se4-block-button-detail" class="se3-block-button">Читати більше геометрії</button>
                </div>
            </div>
        </section>
        <section id="se5-main">
            <h1 class="se5-h1">Математичні лайфхаки</h1>
            <p class="se5-p">Математичні хитрості, корисні поради та алгоритми,<br>
                які допомагають швидше та легше вирішувати задачі, тим самим заощаджуючи час.</p>
            <div class="se5">
                <div class="se5-block">
                    <img class="se5-block-img" src="css/images/numbers.jpg">
                    <h5 class="se5-block-h5">01 /</h5>
                    <p class="se5-block-p">
                        Sample text. Click to select the text box. Click again or double click to start editing the
                        text.
                    </p>
                    <button class="se5-block-button">Детальніше</button>
                </div>
                <div class="se5-block">
                    <img class="se5-block-img" src="css/images/trojan-horse.jpg">
                    <h5 class="se5-block-h5">02 /</h5>
                    <p class="se5-block-p">
                        Sample text. Click to select the text box. Click again or double click to start editing the
                        text.
                    </p>
                    <button class="se5-block-button">Детальніше</button>
                </div>
                <div class="se5-block">
                    <img class="se5-block-img" src="css/images/school-packbag.jpg">
                    <h5 class="se5-block-h5">03 /</h5>
                    <p class="se5-block-p">
                        Sample text. Click to select the text box. Click again or double click to start editing the
                        text.
                    </p>
                    <button class="se5-block-button">Детальніше</button>
                </div>
            </div>
            <div class="se3">
                <div id="se3-block-button-container" class="se3-block">
                    <button id="se5-block-button-detail" class="se3-block-button">Перейти до всіх лайфхаків</button>
                </div>
            </div>
        </section>
        <section id="se6-main">
            <div class="se6">
                <div class="se6-block">
                    <div class="se6-block-h5">
                        <span>600</span>
                    </div>
                    <p class="se6-block-p">
                        Найбільшим числом у світі вважається центильйон, він має 600 нулів.
                    </p>
                </div>
                <div class="se6-block">
                    <div class="se6-block-h5">
                        <span>39000</span>
                    </div>
                    <p class="se6-block-p">
                        Найдавніша математична праця була знайдена не на території Стародавнього
                        Риму або Олександрії і являє собою кістку бабуїна з вибитими на ній рисками, її вік складає
                        майже 40 000 років.
                    </p>
                </div>
                <div class="se6-block">
                    <div class="se6-block-h5">
                        <span>8</span>
                    </div>
                    <p class="se6-block-p">
                        Трьома дотиками ножа торт ділиться на 8 однакових частин. І існує тільки 2 способи для цього.
                    </p>
                </div>
                <div class="se6-block">
                    <div class="se6-block-h5">
                        <span>100000</span>
                    </div>
                    <p class="se6-block-p">
                        Вся математична інформація розміщена у величезній кількості книг: на сьогодні, їх більше
                        100 000.
                    </p>
                </div>
            </div>
            <script src="js/jquery.spincrement.min.js"></script>
            <script src="js/custom.js"></script>
        </section>
    </article>
@endsection
