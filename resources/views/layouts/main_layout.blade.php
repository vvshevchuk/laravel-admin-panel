<!DOCTYPE html>
<html lang="uk">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Maths around us</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css">
    @isset($style)
        <link rel="stylesheet" href="/css/{{ $style }}" type="text/css">
    @endisset
{{--    <link rel="stylesheet" href="/css/main.css" type="text/css">--}}
    <link rel="stylesheet" href="/css/mobile-menu.css" type="text/css">
    <link rel="stylesheet" href="/css/icons.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<!-- ---------- scrollUp ----------------- -->
<div class="scroll-up scroll-up--active">
    <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
        <path
            class="scroll-up__svg-path"
            d="
                    M24,0
                    a24,24 0 0,1 0,48
                    a24,24 0 0,1 0,-48
                "
        />
    </svg>
</div>
<script src="/js/scripts.js"></script>
@include('header')
@yield('main_content')
@include('footer')
</body>
</html>
