<header>

    <div class="social-icons">
        <div class="social-icons-f-y-i">
            <a class="a-social-icons" href="#f"><i class="gg-facebook"></i></a>
            <a class="a-social-icons" href="#y"><i class="gg-youtube"></i></a>
            <a class="a-social-icons" href="#in"><i class="gg-instagram"></i></a>
        </div>
        <div id="mail-icon-email">
            <a id="icon-mail" class="a-social-icons" href="#mail"><i class="gg-mail"></i></a>
            <a id="mail" class="a-social-icons" href="#mail">maths.around.us3.14@gmail.com</a>
        </div>
    </div>
    <div id="top-menu">
        <div id="top-menu-transparent">
            <div id="a">
                <a href="/"><img src="/css/images/logo.jpg" alt="Logo" id="top-menu-logo"></a>
            </div>
            <div id="b">
                <nav class="dws-menu">
                    <!--
                    Теперь добавим в верстку сайта следующие селекторы input и label
                    с использованием checkbox, который будет принимать щелчок  и отображать меню.
                    -->
                    <input type="checkbox" name="toggle" id="menu" class="toggleMenu">
                    <label for="menu" class="toggleMenu"><i class="gg-menu"></i></label>
                    <ul class="menu-main">
                        <li><a href="/"><i class="gg-home-alt"></i>Головна</a></li>
                        <li><a href="{{ route('intro') }}">Вступ</a></li>
                        <li><a href="{{ route('history') }}">Історія математики</a></li>
                        <!--
                        По аналогии как делали с основным меню, практически то же самое делаем с вложенными пунктами.
                        Добавляем к пункту «Алгебра» input и label и опишем стили его отображения.
                        -->
                        <li>
                            <input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m1">
                            <a href="#">Алгебра</a>
                            <label for="sub_m1" class="toggleSubmenu"><i class="fa"></i></label>
                            <ul>
                                <li><a href="numbers">Числа</a></li>
                                <li><a href="#">Рівняння</a></li>
                                <li><a href="#">Нерівності</a></li>
                                <li><a href="#">Системи рівнянь</a></li>
                                <li><a href="#">Комбінаторика</a></li>
                            </ul>
                        </li>
                        <!--
                        Следующий пункт это «Геометрія» и там пропишем данные параметры:
                        -->
                        <li><input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m2">
                            <a href="#">Геометрія</a>
                            <label for="sub_m2" class="toggleSubmenu"><i class="fa"></i></label>
                            <ul>
                                <li><a href="#">Найпростіші геометричні фігури</a></li>
                                <li><a href="#">Трикутник</a></li>
                                <!--
                                По аналогии как делали с основным меню, практически туже операцию
                                нужно провести и с вложенными субменю. Первым делом пропишем input и label в одном из подменю.
                                Первый пункт по списку идет «Одежда», пишем там свою конструкцию и сразу проверяем что получилось:
                                -->
                                <li><input type="checkbox" name="toggle" class="toggleSubmenu" id="sub_m2-1">
                                    <a href="#">Чотирикутник</a>
                                    <label for="sub_m2-1" class="toggleSubmenu"><i class="fa"></i></label>
                                    <ul>
                                        <li><a href="#">Квадрат</a></li>
                                        <li><a href="#">Прямокутник</a></li>
                                        <li><a href="#">Паралелограм</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">Лайфхаки</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
