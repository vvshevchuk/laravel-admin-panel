$(document).ready(function () {

    $(window).on("scroll load resize", function () {
        screenPoint('.se6-block-h5', 100, true, function(){
            $(".se6-block-h5 span").spincrement({
            from: 0,
            to: null,
            thousandSeparator: '',
            duration: 2000,
            fade: true
        });
    })});

    var show = true;

    function screenPoint(elem,offset,infinit,callback){
        var infinit = infinit;
        var countbox = elem;
        var offset = offset;
        var func = callback || function(){};
        var w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
        var e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
        var w_height = $(window).height(); // Высота окна браузера
        var d_height = $(document).height(); // Высота всего документа
        var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
        if (infinit) {
            if (w_top + w_height < e_top && show == false || w_top > e_top + e_height && show == false)
            show = true;
        }

        if (!show) return false;

        if (w_top + w_height - offset >= e_top && w_top + offset < e_top + e_height) {
            func();
            show = false;
        }
    }
});
