<?php

use App\Http\Controllers\Admin\SubjectController;
use App\Http\Controllers\Admin\LessonController;
use App\Http\Controllers\Admin\ChapterController;
use App\Http\Controllers\Admin\CourseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/intro', function () {
    return view('intro', [
        'style' => 'intro.css'
    ]);
})->name('intro');

Route::get('/history', function () {
    return view('history', [
        'style' => 'history.css'
    ]);
})->name('history');

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware(['role:admin'])->prefix('admin_panel')->group(function () {
    Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('homeAdmin'); // /admin
    Route::resource('subject', SubjectController::class);
    Route::resource('lesson', LessonController::class);
    Route::resource('chapter', ChapterController::class);
    Route::resource('course', CourseController::class);
});
