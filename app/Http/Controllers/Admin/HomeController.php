<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Models\Lesson;
use App\Models\Chapter;
use App\Models\Course;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $lessons_count = Lesson::all()->count();
        $subjects_count = Subject::all()->count();
        $courses_count = Course::all()->count();
        $chapters_count = Chapter::all()->count();

        return view('admin.home.index', [
            'lessons_count' => $lessons_count,
            'subjects_count' => $subjects_count,
            'courses_count' => $courses_count,
            'chapters_count' => $chapters_count,
        ]);
    }
}
