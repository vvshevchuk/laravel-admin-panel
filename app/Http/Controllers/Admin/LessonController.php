<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Models\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::orderBy('created_at', 'DESC')->get();

        return view('admin.lesson.index', [
            'lessons' => $lessons
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::orderBy('created_at', 'DESC')->get();

        return view('admin.lesson.create', [
            'subjects' => $subjects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lesson = new Lesson();
        $lesson->title = $request->title;
        $lesson->img = $request->img;
        $lesson->text = $request->text;
        $lesson->sub_id = $request->sub_id;
        $lesson->save();

        return redirect()->back()->withSuccess('Урок успішно додано');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        $subjects = Subject::orderBy('created_at', 'DESC')->get();

        return view('admin.lesson.edit', [
            'subjects' => $subjects,
            'lesson' => $lesson,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesson $lesson)
    {
        $lesson->title = $request->title;
        $lesson->img = $request->img;
        $lesson->text = $request->text;
        $lesson->sub_id = $request->sub_id;
        $lesson->save();

        return redirect()->back()->withSuccess('Урок успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        $deleteLesson = $lesson->title;
        $lesson->delete();
        return redirect()->back()->withSuccess("Урок \"{$deleteLesson}\" успішно видалено!");
    }
}
