<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use Illuminate\Http\Request;
use App\Models\Chapter;
use App\Models\Course;
class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chapters = Chapter::orderBy('created_at', 'desc')->get();

        return view('admin.chapter.index', [
            'chapters' => $chapters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::orderBy('created_at', 'DESC')->get();

        return view('admin.chapter.create', [
            'courses' => $courses
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_chapter = new Chapter();
        $new_chapter->title = $request->title;
        $new_chapter->course_id = $request->course_id;
        $new_chapter->save();

        return redirect()->back()->withSuccess('Розділ успішно додано!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter)
    {
        $courses = Course::orderBy('created_at', 'DESC')->get();

        return view('admin.chapter.edit', [
            'courses' => $courses,
            'chapter' => $chapter,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapter $chapter)
    {
        $chapter->title = $request->title;
        $chapter->course_id = $request->course_id;
        $chapter->save();

        return redirect()->back()->withSuccess('Розділ успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter)
    {
        $deleteChapter=$chapter->title;
        $chapter->delete();
        return redirect()->back()->withSuccess("Розділ \"{$deleteChapter}\" успішно видалено!");
    }
}
