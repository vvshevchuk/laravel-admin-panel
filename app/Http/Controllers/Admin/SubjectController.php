<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::orderBy('created_at', 'desc')->get();

        return view('admin.subject.index', [
            'subjects' => $subjects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chapters = Chapter::orderBy('created_at', 'DESC')->get();

        return view('admin.subject.create', [
            'chapters' => $chapters
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_subject = new Subject();
        $new_subject->title = $request->title;
        $new_subject->chap_id = $request->chap_id;
        $new_subject->save();

        return redirect()->back()->withSuccess('Тему успішно додано!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        $chapters = Chapter::orderBy('created_at', 'DESC')->get();

        return view('admin.subject.edit', [
            'chapters' => $chapters,
            'subject' => $subject,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $subject->title = $request->title;
        $subject->chap_id = $request->chap_id;
        $subject->save();

        return redirect()->back()->withSuccess('Тему успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $deleteSubject=$subject->title;
        $subject->delete();
        return redirect()->back()->withSuccess("Тему \"{$deleteSubject}\" успішно видалено!");
    }
}
