<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Lesson;
use App\Models\Chapter;
use App\Models\Course;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $lessons= Lesson::all();
        $subjects = Subject::all();
        $chapters = Chapter::all();
        $courses = Course::all();

        return view('home',[
            'style' => 'home.css',
            'lessons' => $lessons,
            'subjects' => $subjects,
            'chapters' => $chapters,
            'courses' => $courses,
        ]);
    }
}
