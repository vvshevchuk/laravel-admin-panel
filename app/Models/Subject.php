<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    public function lessons()
    {
        return $this->hasMany('App\Models\Lesson', 'sub_id', 'id');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter', 'chap_id', 'id');
    }
}
